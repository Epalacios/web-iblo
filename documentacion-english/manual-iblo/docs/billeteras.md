#  Wallets Module

This module allows us to view the wallets imported to the user and also import accepted wallets (Wallet Trezor, Wallet Legger, and other non-custodial walls).

<center>
![Menú Actualizaciones](/img/menu_billetera.png "Menú Actualizaciones")
</center>

## Módulo de My Public Keys

Next, the user must enter the requested data to import their wallet

<center>
![Menú Actualizaciones](/img/billetera1.png "Menú Actualizaciones")
</center>

Once the data has been entered, the user must click on the import button

If the import was successful, the data will appear on the User Wallet Information screen.

The public keys have to be xpub.

**Note: The Iblo Dashboard platform does not store private keys.**

<center>
![Menú Actualizaciones](/img/billetera2.png "Menú Actualizaciones")
</center>

By clicking on the icon in the actions section, decrypt the public key linked to the wallet.

<center>
![Menú Actualizaciones](/img/billetera3.png "Menú Actualizaciones")
</center>