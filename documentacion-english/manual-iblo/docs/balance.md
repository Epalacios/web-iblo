#  Balance Module

This module indicates the available balance in each Wallet associated with the Iblo Dashboard platform.

In addition, it lists the set of operations carried out in a given period of time, it shows the balance of the selected Wallet.

<center>
![Menú Actualizaciones](/img/blance.png "Menú Actualizaciones")
</center>

When placing the mouse pointer over the icon of the see detail section and clicking, a screen shows a detail of the operation.

Indicating the ID of the operation, the Hash, the block number, the amount paid expressed in dollars and the change to BTC, the exchange rate, the date and status of the operation.

<center>
![Menú Actualizaciones](/img/balance5.png "Menú Actualizaciones")
</center>

This section allows us to list the information of the operations by number of rows.

<center>
![Menú Actualizaciones](/img/balance4.png "Menú Actualizaciones")
</center>

This section allows us to list the specific columns that the user needs to see.

<center>
![Menú Actualizaciones](/img/balance3.png "Menú Actualizaciones")
</center>

This section allows us to obtain information through a search engine which will filter the information by any of the columns.

<center>
![Menú Actualizaciones](/img/balance6.png "Menú Actualizaciones")
</center>

In addition, when clicking on the copy section, we can copy all the data of the operations carried out in the user's clipboard.

Finally, when clicking on the export PDF section, all the data of the operations carried out is downloaded in a PDF.