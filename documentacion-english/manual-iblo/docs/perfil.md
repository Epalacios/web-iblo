# My Profile Module
<center>
![Menú Actualizaciones](/img/menu_perfil.png "Menú Actualizaciones")
</center>

## User Module

It allows you to update previously registered data such as: The name of your business and The contact name.

Note: To update the data it is necessary to enter the password. If it is a different password than the one you currently have, it will be changed

<center>
![Menú Actualizaciones](/img/usuario.png "Menú Actualizaciones")
</center>


This screen displays data such as: Email, Wallet Name, User Name, and specifies the type of plan that the user has.

In addition, it allows us to upload a user profile photo.

<center>
![Menú Actualizaciones](/img/usuario2.png "Menú Actualizaciones")
</center>

To upload a user profile photo, just click on the user image.

**Note: The format for the profile picture is 400x100 pixels.**

## Subscription Module

Next, the type of plan that the user has is displayed, specifying the characteristics of said plan.

<center>
![Menú Actualizaciones](/img/suscripcion.png "Menú Actualizaciones")
</center>

In this section the Payment History is displayed, where we can find the subscription that was purchased, the type of payment of the operation, the amount of the operation, the date of payment of the operation, the reference number and the status of the operation.

<center>
![Menú Actualizaciones](/img/suscripcion2.png "Menú Actualizaciones")
</center>