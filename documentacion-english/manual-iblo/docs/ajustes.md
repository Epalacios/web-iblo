#  Settings Module

<center>
![Menú Actualizaciones](/img/ajustes.png "Menú Actualizaciones")
</center>

This configuration allows you to select how many confirmations the BTC payment validation service should wait for in order to change a payment status to a completed payment.

<center>
![Menú Actualizaciones](/img/ajustes2.png "Menú Actualizaciones")
</center>