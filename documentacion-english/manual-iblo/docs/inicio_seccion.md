# Login to the System
To access the Iblo Dashboard platform, enter your Username or Email and Password.
<center>
![Menú Actualizaciones](/img/login.png "Menú Actualizaciones")
</center>

Once you have entered your username and password, the platform verifies your data.

<center>
![Menú Actualizaciones](/img/revision.png "Menú Actualizaciones")
</center>

If your data is correct, the platform gives you access to the system.

If the user is new to the Iblo Dashboard platform, the following screen will be displayed, specifying that the user does not have an associated wallet.

<center>
![Menú Actualizaciones](/img/wallet.png "Menú Actualizaciones")
</center>

Clicking on the Import Wallet button redirects us to the Wallet section (My public keys) where the user's wallet will be imported