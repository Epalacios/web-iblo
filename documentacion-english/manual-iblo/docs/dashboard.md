# Dashboard module

Entering the system on the left side can display a menu. It is displayed dynamically, that is, according to the permissions assigned to each user.
<center>
![Menú](/img/menu.png "Menú")
</center>

This module summarizes the most relevant data (My payments, Completed payments, Pending payments, Amount of payments per month, Number of payments per month).

The following screen displays the amount received in dollars and BTC, in addition to the pending amount to be received in dollars and the pending amount to be received in BTC.

<center>
![Menú](/img/mispagos.png "Menú")
</center>

The following screen displays the number of completed payments, the total amount received in dollars and in BTC.

<center>
![Menú](/img/pagos_completados.png "Menú")
</center>

On the next screen, the amount of pending payments is specified, as well as the money received in dollars and BTC from said pending payments.

<center>
![Menú](/img/pagos_pendientes.png "Menú")
</center>

On the next screen a graph is displayed specifying the amount received per month.
<center>
![Menú](/img/grafica_pagos.png "Menú")
</center>

On the next screen, a bar graph is displayed specifying the number of payments received per month.

<center>
![Menú](/img/grafica_pagos2.png "Menú")
</center>

Next, this screen specifies the last 5 transactions received, displaying where you can see the progress and status of each one of them.

<center>
![Menú](/img/inicio_2.png "Menú")
</center>