#  Withdrawal Module

This module handles Wallet Balance withdrawals on the Iblo Dashboard platform.

Note: It should be noted that the sending BTC Wallet must be a Trezor BTC wallet.

To make withdrawals from the Wallet Balance on the Iblo Dashboard platform, the following steps must be followed:

Step 1: Select the sending BTC Wallet (only available through trezor).

<center>
![Menú Actualizaciones](/img/retiro.png "Menú Actualizaciones")
</center>

Step 2: Enter a valid withdrawal wallet.

<center>
![Menú Actualizaciones](/img/retiro.png "Menú Actualizaciones")
</center>

Step 3: Enter the withdrawal amount.

If you want to withdraw all your funds you can click the MAX button

<center>
![Menú Actualizaciones](/img/retiro.png "Menú Actualizaciones")
</center>

Step 4: Click the withdrawal button.

<center>
![Menú Actualizaciones](/img/retiro.png "Menú Actualizaciones")
</center>

Finally, the application will redirect to the Trezor portal where you must follow the steps of said application to complete the withdrawal.

Note: this last application is external to Iblo and any detail or error does not depend on our application.

<center>
![Menú Actualizaciones](/img/trezor.png "Menú Actualizaciones")
</center>