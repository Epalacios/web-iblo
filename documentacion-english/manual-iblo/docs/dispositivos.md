# Devices Module

<center>
![Menú Actualizaciones](/img/menu_dispositivo.png "Menú Actualizaciones")
</center>

# POS module

It lists the data of the wallets registered in the Iblo platform, it also lists all the addresses registered in Iblo and which ones are associated with a POS device (point of sales).

<center>
![Menú Actualizaciones](/img/dispositivo1.png "Menú Actualizaciones")
</center>


Placing the mouse pointer over the green button will display this message.

<center>
![Menú Actualizaciones](/img/dispositivo2.png "Menú Actualizaciones")
</center>

If the user wishes to unlink the linked device, they should only press the green button.

Remember that when you enter the Iblo Dashboard application again on your phone, your device will be unlinked. If you want to enter you will need to pair the device again.

Placing the mouse pointer over the device icon will display this message, it specifies the linked device model.

<center>
![Menú Actualizaciones](/img/dispositivo3.png "Menú Actualizaciones")
</center>


