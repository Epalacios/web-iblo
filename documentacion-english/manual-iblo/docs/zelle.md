#  Zelle module

<center>
![Menú Actualizaciones](/img/menu_zelle.png "Menú Actualizaciones")
</center>

## Zelle Registration Module

This module requests a Zelle email record which will be used to validate the payments made through this means in a virtual gateway.

To register for Zelle mail, you must enter your email, password, and a label to easily identify the mail.

<center>
![Menú Actualizaciones](/img/zelle1.png "Menú Actualizaciones")
</center>


## My Payments Module in Zelle

Below is the History of Payments received in the previously registered Zelle mail, which were validated in the virtual gateway, displaying data of the operation such as: The reference number, the description, the amount, the total amount, the Invoice ID, status and date of operation.

<center>
![Menú Actualizaciones](/img/zelle2.png "Menú Actualizaciones")
</center>
