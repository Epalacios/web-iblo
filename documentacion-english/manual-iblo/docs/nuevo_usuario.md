## New User Registration Module

To register, complete the requested data: Name of the business, Contact name, Username, Email and a password in the format allowed by the application.

**password format: At least 8 characters, at least one uppercase, at least one lowercase, at least one number and at least one special character (!@#$%&*).**

<center>
![Menú Actualizaciones](/img/nuevo_usuario.png "Menú Actualizaciones")
</center>

Once you have entered all the requested data and agree with the Privacy Policies, Terms and Conditions, select the Register User button.

Next, the Iblo Dashboard Platform sends a message to the previously registered email to activate the account.

<center>
![Menú Actualizaciones](/img/correo.png "Menú Actualizaciones")
</center>

By pressing the Activate Account button, your account will be active and you will be able to access Iblo Dashboard.