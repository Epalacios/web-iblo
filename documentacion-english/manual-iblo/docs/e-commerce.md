#  Ecommerce module

<center>
![Menú Actualizaciones](/img/menu_ecommerce.png "Menú Actualizaciones")
</center>


#  API module

This module allows the user to change the redirect routes when using the BTC payment gateway (gateway that is generated when the user pays by SMS, email and the payment button.)

It also allows you to copy the token to integrate with other Iblo products

<center>
![Menú Actualizaciones](/img/ecommerce.png "Menú Actualizaciones")
</center>

It is requested that I specified the URL on failure and specified the URL on success.

# Widget module

This module allows us to generate a payment button that can be implemented on pages external to Iblo Dasboard.
 
To generate a payment button with the Iblo platform, the following steps must be followed:

Step 1: Select the wallet.

<center>
![Menú Actualizaciones](/img/paso1.png "Menú Actualizaciones")
</center>

Step 2: Select the Type of Button (Variable Price or Fixed Price).

In case of generating a payment button (Variable price).

<center>
![Menú Actualizaciones](/img/paso2.png "Menú Actualizaciones")
</center>

Step 3 will be: Click the Generate button.

<center>
![Menú Actualizaciones](/img/paso3.png "Menú Actualizaciones")
</center>

To finally have the payment button with the Iblo platform.

<center>
![Menú Actualizaciones](/img/pantalla_final.png "Menú Actualizaciones")
</center>

For the payment to be variable, you must change the value of the hidden field that has the value of id="smartbutton-cryptoesb-amount". The default value of this field is 1 and according to the value it has, the equivalent payment amount in BTC will be generated.

<center>
![Menú Actualizaciones](/img/pantalla_final_2.png "Menú Actualizaciones")
</center>

In case of generating a payment button (Fixed Price).

Step 3 will be: Enter the amount.

<center>
![Menú Actualizaciones](/img/paso3_1.png "Menú Actualizaciones")
</center>

Step 4 will be: Click on Generate Button.

<center>
![Menú Actualizaciones](/img/paso4.png "Menú Actualizaciones")
</center>

To finally have the payment button with the Iblo platform.

<center>
![Menú Actualizaciones](/img/pantalla_final_1.png "Menú Actualizaciones")
</center>


Clicking the copy code button will allow you to obtain the HTML code, which can be implemented in any standard HTML page.