## Account module

<center>
![Menú Actualizaciones](/img/cuenta.jpeg "Menú Actualizaciones")
</center>

By clicking on the account module, a menu will be displayed specifying user data such as: (Email, Current Wallet, Device ID). In addition there are three modules (Recover your password, Subscription and Close session).

<center>
![Menú Actualizaciones](/img/menu_derecho.png "Menú Actualizaciones")
</center>

## Module to recover your account

The module to recover your account allows you to change your password in case you forget it.

You will need to enter the email associated with the iblo account.

<center>
![Menú Actualizaciones](/img/recuperar.jpeg "Menú Actualizaciones")
</center>

The application will then send a link to change your password to your email.

<center>
![Menú Actualizaciones](/img/recuperar1.png "Menú Actualizaciones")
</center>

You will need to enter your new password to access the application. This password must meet the following validations.

**Password format: At least 8 characters, at least one uppercase letter, at least one lowercase letter, at least one number and at least one special character (!@#$%&*).**


<center>
![Menú Actualizaciones](/img/recuperar2.png "Menú Actualizaciones")
</center>

## Subscription Module

The subscription module shows us the available plans and the operation limits for a user.

To subscribe, you must press the Subscribe button.

<center>
![Menú Actualizaciones](/img/plan2.jpeg "Menú Actualizaciones")
</center>

You must indicate your payment method (PayPal, Debit or Credit Card)

<center>
![Menú Actualizaciones](/img/plan3.jpeg "Menú Actualizaciones")
</center>

If you have indicated Paypal, you must enter your access credentials to the platform.

<center>
![Menú Actualizaciones](/img/plan4.jpeg "Menú Actualizaciones")
</center>

You will need to choose your payment method.

<center>
![Menú Actualizaciones](/img/plan6.jpeg "Menú Actualizaciones")
</center>

Next, a payment confirmation screen is displayed.

<center>
![Menú Actualizaciones](/img/plan5.jpeg "Menú Actualizaciones")
</center>

You will have to wait a few seconds

<center>
![Menú Actualizaciones](/img/plan7.jpeg "Menú Actualizaciones")
</center>

Once the payment is confirmed, the user will subscribe to the selected plan.
<center>
![Menú Actualizaciones](/img/plan8.jpeg "Menú Actualizaciones")
</center>

Finally, your active plan will be displayed.

<center>
![Menú Actualizaciones](/img/plan1.jpeg "Menú Actualizaciones")
</center>

If you have indicated a Debit or Credit Card, you must enter all the data requested to carry out the operation.

<center>
![Menú Actualizaciones](/img/tarjeta.jpeg "Menú Actualizaciones")
</center>