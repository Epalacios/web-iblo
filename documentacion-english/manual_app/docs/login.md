# System Login Module

To access the Iblo mobile application, enter your Username or Email and Password.

<center>
![Menú Actualizaciones](/img/login.jpeg "Menú Actualizaciones")
</center>

Once you have entered the requested data, you must click on login.

If your data is correct, the platform gives you access to the system.

<center>
![Menú Actualizaciones](/img/inicio_sesion.jpeg "Menú Actualizaciones")
</center>

If the user is new to the Iblo App platform, the following screen will be displayed, specifying that the user does not have an associated or imported wallet.

To create a Wallet from the application you must click on the Create button

<center>
![Menú Actualizaciones](/img/importar_wallet.jpeg "Menú Actualizaciones")
</center>

The application shows a screen confirming if you want to create a new Wallet, you must click on the Continue button.

<center>
![Menú Actualizaciones](/img/importar_wallet2.jpeg "Menú Actualizaciones")
</center>

**Step 1**: It is responsible for generating the seed words, you must save them in the specified order. To continue with the process, you must click on the Continue button.

<center>
![Menú Actualizaciones](/img/importar_wallet3.jpeg "Menú Actualizaciones")
</center>

The application sends a confirmation screen, you will have to confirm that you saved the seed words and press the Continue button

<center>
![Menú Actualizaciones](/img/importar_wallet4.jpeg "Menú Actualizaciones")
</center>

**Step 2**: It is responsible for confirming the seed phrases, you must include these seed words in the same order specified in the previous step.

<center>
![Menú Actualizaciones](/img/importar_wallet5.jpeg "Menú Actualizaciones")
</center>

**Step 3**: It is responsible for requesting the name of the Wallet label and the password.

**Wallet Label**: Common recognition name of the Wallet in the App.

<center>
![Menú Actualizaciones](/img/importar_wallet6.jpeg "Menú Actualizaciones")
</center>

When clicking on the Create Wallet button, you must wait a few seconds for the creation of the Wallet

<center>
![Menú Actualizaciones](/img/importar_wallet7.jpeg "Menú Actualizaciones")
</center>

Once the Wallet has been created, the application will enter Home to start using the active functions in the App. Every time you enter Iblo App from a terminal, it will connect to the associated Wallet.

<center>
![Menú Actualizaciones](/img/home.jpeg "Menú Actualizaciones")
</center>

**Note**: If you want to restore the Wallet in any other application, you must have at hand the seed words indicated above, if these seed words are lost, the Wallet cannot be restored, therefore, the responsibility for the custody of the seed is of the user who is registering in the App, Iblo does not save any private key of any Wallet.