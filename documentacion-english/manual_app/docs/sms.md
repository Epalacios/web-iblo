# SMS module

<center>
![Menú Actualizaciones](/img/SMS.jpeg "Menú Actualizaciones")
</center>

# Payment module by text message (sms).

This module is responsible for generating a payment of BTC by text message (SMS)

To make a payment by text message (SMS) you will need to enter the phone number (only U.S.A. phone numbers are accepted) and the amount in dollars or BTC.

<center>
![Menú Actualizaciones](/img/pago_sms.jpeg "Menú Actualizaciones")
</center>

Once you have entered the phone number and the amount, you must click on send button, the Iblo mobile application will show the following screen requesting confirmation.

If the user agrees with the payment request, they must click on send button

<center>
![Menú Actualizaciones](/img/pago_sms1.jpeg "Menú Actualizaciones")
</center>

Next, the application will send a message to the previously indicated number.

<center>
![Menú Actualizaciones](/img/pago_sms22.jpeg "Menú Actualizaciones")
</center>

If the message was sent successfully, this message will be displayed.

<center>
![Menú Actualizaciones](/img/correo_electronico2.jpeg "Menú Actualizaciones")
</center>

The user must enter the message tray and find a text message from Iblo with the payment request.

<center>
![Menú Actualizaciones](/img/pago_sms2.jpeg "Menú Actualizaciones")
</center>


You must click on the Pay button, once you press the button you will be sent to the Iblo payment gateway.

<center>
![Menú Actualizaciones](/img/correo_electronico6.png "Menú Actualizaciones")
</center>

You will have a period of 30:00 minutes to make the payment.

Once the payment is verified, this message will be displayed.

<center>
![Menú Actualizaciones](/img/correo_electronico7.png "Menú Actualizaciones")
</center>