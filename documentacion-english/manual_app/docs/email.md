# Email module

<center>
![Menú Actualizaciones](/img/correo_electronico.jpeg "Menú Actualizaciones")
</center>

# Email Payment Module

This module is responsible for generating a BTC payment via email.

To make a payment we must enter the email and the amount in dollars or in BTC.

<center>
![Menú Actualizaciones](/img/correo_electronico1.jpeg "Menú Actualizaciones")
</center>

Once the email has been entered and the amount must be clicked on send, the Iblo mobile application will show the following screen requesting confirmation.

If the user agrees with the payment request, they must click on send

<center>
![Menú Actualizaciones](/img/correo_electronico4.jpeg "Menú Actualizaciones")
</center>

Next, the application will send a message to the previously indicated email address.

<center>
![Menú Actualizaciones](/img/correo_electronico3.jpeg "Menú Actualizaciones")
</center>

If the email was sent successfully, this message will be displayed.

<center>
![Menú Actualizaciones](/img/correo_electronico2.jpeg "Menú Actualizaciones")
</center>

The user must enter the previously indicated email and will find an email from Iblo with the payment request.

<center>
![Menú Actualizaciones](/img/correo_electronico5.png "Menú Actualizaciones")
</center>

You must click on the Pay button, once you press the button you will be sent to the Iblo payment gateway.

<center>
![Menú Actualizaciones](/img/correo_electronico6.png "Menú Actualizaciones")
</center>

You will have a period of 30:00 minutes to make the payment.

Once the payment is verified, this message will be displayed.

<center>
![Menú Actualizaciones](/img/correo_electronico7.png "Menú Actualizaciones")
</center>