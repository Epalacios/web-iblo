To access the Iblo mobile application, enter your Username or Email and Password.

If the user does not have an account in Iblo App, they must click on the link Don't have an account yet?

<center>
![Menú Actualizaciones](/img/login.jpeg "Menú Actualizaciones")
</center>

Where the new user registration module will be rooted

## New User Registration Module

To register, complete the requested data: Name of the business, Contact name, Username, Email and a password in the format allowed by the application.

**Password format: At least 8 characters, at least one uppercase letter, at least one lowercase letter, at least one number and at least one special character (!@#$%&*).**

<center>
![Menú Actualizaciones](/img/nuevo_usuario.jpeg "Menú Actualizaciones")
</center>

Once you have entered all the requested data and agree with the Privacy Policies, Terms and Conditions, select the Register User button.

Next, the Iblo Dashboard Platform will send a message to the previously registered email to activate the account.

<center>
![Menú Actualizaciones](/img/correo.png "Menú Actualizaciones")
</center>

By pressing the Activate Account button, your account will be active and you will be able to access the Iblo App.