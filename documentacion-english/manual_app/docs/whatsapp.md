# WhatsApp module

<center>
![Menú Actualizaciones](/img/whatsapp.jpeg "Menú Actualizaciones")
</center>

# WhatsApp payment module

This module is responsible for generating a payment of BTC by WhatsApp.

To make a payment by WhatsApp you must enter the phone number and the amount in dollars or in BTC.

<center>
![Menú Actualizaciones](/img/whatsapp2.jpeg "Menú Actualizaciones")
</center>

Once you have entered the phone number and the amount, you must click on send button, the Iblo mobile application will show the following screen requesting confirmation.

If the user agrees with the payment request, they must click send button

<center>
![Menú Actualizaciones](/img/whatsapp3.jpeg "Menú Actualizaciones")
</center>

Next, the application will send a message to the previously indicated WhatsApp number.

<center>
![Menú Actualizaciones](/img/whatsapp4.jpeg "Menú Actualizaciones")
</center>

If the message was sent successfully, this message will be displayed.

<center>
![Menú Actualizaciones](/img/correo_electronico2.jpeg "Menú Actualizaciones")
</center>

The user must enter WhatsApp and find an Iblo WhatsApp with the payment request.

<center>
![Menú Actualizaciones](/img/whatsapp5.jpeg "Menú Actualizaciones")
</center>

You must click on the Pay button, once you press the button you will be sent to the Iblo payment gateway.

<center>
![Menú Actualizaciones](/img/correo_electronico6.png "Menú Actualizaciones")
</center>

You will have a period of 30:00 minutes to make the payment.

Once the payment is verified, this message will be displayed.

<center>
![Menú Actualizaciones](/img/correo_electronico7.png "Menú Actualizaciones")
</center>