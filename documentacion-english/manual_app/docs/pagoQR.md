# QR payment module

<center>
![Menú Actualizaciones](/img/pagoQR.jpeg "Menú Actualizaciones")
</center>

This module is responsible for generating a BTC payment through a QR.

To make a payment we must enter the amount in dollars or in BTC.

<center>
![Menú Actualizaciones](/img/pago_QR1.jpeg "Menú Actualizaciones")
</center>

Once the amount to be paid has been entered, the Verify Payment button must be clicked.

<center>
![Menú Actualizaciones](/img/verificar.jpeg "Menú Actualizaciones")
</center>

If the payment was made successfully, the following screen will be displayed.

<center>
![Menú Actualizaciones](/img/pago.png "Menú Actualizaciones")
</center>

When verifying the payment I pressed the Ok button, the application will return to the module of receiving payment by QR.