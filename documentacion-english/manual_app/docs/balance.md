# Balance Module

<center>
![Menú Actualizaciones](/img/balance.jpeg "Menú Actualizaciones")
</center>

The balance module allows viewing the name of the Wallet as well as the last 10 payments received and requested.

<center>
![Menú Actualizaciones](/img/balance1.jpeg "Menú Actualizaciones")
</center>

This button allows us to see details of the operation.

<center>
![Menú Actualizaciones](/img/balance_boton.jpeg "Menú Actualizaciones")
</center>

Clicking the button will display a pop-up screen that will look like the following:

1. Payments received (QR)

<center>
![Menú Actualizaciones](/img/balance3.jpeg "Menú Actualizaciones")
</center>

2. Payments sent (Email, SMS, WhatsApp)

<center>
![Menú Actualizaciones](/img/balance2.jpeg "Menú Actualizaciones")
</center>