# Hard Wallet Module
<center>
![Menú Actualizaciones](/img/hard_wallet.jpeg "Menú Actualizaciones")
</center>

In case when starting the section in the Iblo App and not having created a wallet, since you had a Wallet previously imported through Iblo Dashboard, this functionality will allow you to create a Hard Wallet

To create a Wallet from the application you must click on the Create button

<center>
![Menú Actualizaciones](/img/importar_wallet.jpeg "Menú Actualizaciones")
</center>

The application shows a screen confirming if you want to create a new Wallet, you must click on the Continue button.

<center>
![Menú Actualizaciones](/img/importar_wallet2.jpeg "Menú Actualizaciones")
</center>

Step 1 is responsible for generating the seed words, you must save them in the specified order to continue with the next step, you must click on the Continue button.

<center>
![Menú Actualizaciones](/img/importar_wallet3.jpeg "Menú Actualizaciones")
</center>

The application sends a confirmation screen, you must click the Continue button

<center>
![Menú Actualizaciones](/img/importar_wallet4.jpeg "Menú Actualizaciones")
</center>

Step 2 takes care of confirming the seed phrases in the specific order given in the previous step.

<center>
![Menú Actualizaciones](/img/importar_wallet5.jpeg "Menú Actualizaciones")
</center>

Step 3 takes care of requesting the Wallet tag name and password.

<center>
![Menú Actualizaciones](/img/importar_wallet6.jpeg "Menú Actualizaciones")
</center>

When clicking on the Create Wallet button, you must wait a few seconds for the creation of the Wallet

<center>
![Menú Actualizaciones](/img/importar_wallet7.jpeg "Menú Actualizaciones")
</center>

