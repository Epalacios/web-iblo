# Introducci&oacute;n
<div class="img-responsive img-fluid">
<img src="https://iblo.app/assets/img/logos/logo-iblo.png"> 
</div>


## <span id="api-intro-whatsCryptoesb">¿Qué es Iblo?</span>

Comienza a recibir cripto en tu negocio, de forma fácil y rápida sin intermediarios con nuestra tecnología sin custodia, IBLO es una aplicación C2P y POS que te permite recibir solicitudes de pago por correo electrónico, SMS o código QR.

## <span id="api-intro-httpStatus">Códigos de estado HTTP</span>

Cuando consultas Iblo te puede retornar distintos códigos de estado HTTP. Entre los códigos de estados que te puedes encontrar en Iblo están.

**Status 200:** Esto significa que la consulta fue completada exitosamente, cuando esto sucede Iblo retorna un Json con un `{"status": "success"}`, junto a los valores correspondiente a la consulta.

 
**Status 204-2XX:** Todos los códigos de estado 2XX significan que la consulta se completó, pero con errores.

**Status 4XX:** Estos códigos corresponden un error por parte del cliente como por ejemplo no tener acceso a internet, escribir mal una dirección URL o hacer una consulta errónea, entre otras. Cuando esto sucede retorna json `{"status":"error","mensage":"method does not exist}"`   cuando se consulta un método inexistente y `{"status":"error","mensage":"problems with the request, please check"}` cuando hay un error en los parámetros.

**Status 5XX:** Estos errores corresponden a errores por parte de servidor, cuando esto sucede retorna un json `{"status": "error","message": "problems with the request, please check"}`.

## <span id="api-intro-resource">Recursos de Iblo</span>

Todos los recursos que posee Iblo están distribuido en los siguientes esquemas: Para producción: `https://api.iblo.app/testnet/index.php/Api/pos/Api/[path]` , para pruebas y certificación: `https://api.iblo.app/index.php/Api/pos/Api/[path]`, donde [path] es el método a consultar. Aquí esta una lista donde se presentan los distintos servicios con sus respectivos recursos.



