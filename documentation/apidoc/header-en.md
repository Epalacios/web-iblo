# Introduction
<div class="img-responsive img-fluid">
<img src="https://iblo.app/assets/img/logos/logo-iblo.png"> 
</div>


## <span id="api-intro-whatsCryptoesb">What is Iblo?</span>

Start receiving crypto in your business, easily and quickly without intermediaries with our non-custodial technology, IBLO is a C2P and POS application that allows you to receive payment requests by email, SMS or QR code.

## <span id="api-intro-httpStatus">HTTP status codes</span>

When you query Iblo it can return different HTTP status codes. Among the state codes that you can find in Iblo are.

**Status 200:** This means that the query was completed successfully, when this happens Iblo returns a Json with a `{"status": "success"}`, next to the values corresponding to the query.

 
**Status 204-2XX:** All 2XX status codes mean the query completed, but with errors.

**Status 4XX:** These codes correspond to an error on the part of the client, such as not having access to the internet, typing a wrong URL address or making a wrong query, among others. When this happens it returns json `{"status":"error","mensage":"method does not exist}"`  when a nonexistent method is queried and `{"status":"error","mensage":"problems with the request, please check"}` when there is an error in the parameters.

**Status 5XX:** These errors correspond to errors by the server, when this happens it returns a json `{"status": "error","message": "problems with the request, please check"}`.

## <span id="api-intro-resource">Iblo Resources</span>

All the resources that Iblo owns are distributed in the following schemes: For production: `https://api.iblo.app/testnet/index.php/Api/pos/Api/[path]` , for testing and certification: `https://api.iblo.app/index.php/Api/pos/Api/[path]`, donde [path] is the method to query. Here is a list where the different services are presented with their respective resources.



