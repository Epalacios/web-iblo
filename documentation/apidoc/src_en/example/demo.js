/**

 * @api {POST} /get_token_auth?lang=es&/ Get Token

 * @apiVersion 0.0.1
 * @apiName GetToken
 * @apiGroup Token
 * 

 * @apiDescription Rest Full web service of type POST, allows to obtain an authorization token.
 * @apiHeader {String} Content-Type Media type of the resource.
 * @apiHeader (Params) {String} [lang] API response language (es = Spanish , en = English).
 * 

 * @apiBody {String}   username     User authorized to the application.
 * @apiBody {String}   password     Password registered in the application.
 * 

 * 

 * @apiSuccess {Boolean} success Indicates the status of the true or false response.
 * @apiSuccess {String}  token   alphanumeric code that corresponds to the authorization token in the application.
 *

 * @apiError (Error 401 Unauthorized) {Boolean} success Request response status.
 * @apiError (Error 401 Unauthorized) {String}  message Error message indicated by the request.
 * 

 * 

 * @apiError (Error 503 Service Unavailable) {Boolean} success Request response status.
 * @apiError (Error 503 Service Unavailable) {String}  message Error message indicated by the request.
 * 

 * @apiErrorExample Success 200 (Example):
 *     {

 *   "success": true,

 *   "token": "oyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpZCI6IjI0IiwidXNlcm5hbWUiOiJtYXJjb3N0ZXN0MDEiLCJpYXQiOjE2NTIwMTk3MDUsImV4cCI6MTY4MzEyMzcwNX0.5DFWtdBi_lSW3VaEzWDOiKs8vkn6rIXEaRyJLay59PU"

 *   }

 * @apiErrorExample Error 401 (Example1):
 *    {

 *  "success": false,

 *  "message": "Invalid User/Password"

 *   }

 * @apiErrorExample Error 401 (Example2):
 *    {

 *   "success": false,

 *    "message": "Inactive account, please check your email and activate your account"

 *   }

 * @apiErrorExample Error 503 (Example3):
 *    {

 *   "success": false,

 *   "message": "Error processing request, please try again later"

 *   }

 * 

 */

function getUser() { return; }

/**

 * @api {post} /get_payments_pos?lang=es List Payments

 * @apiVersion 0.0.1
 * @apiName Payments
 * @apiGroup List of payments
 

 *

 * @apiDescription Web service that allows you to list payments by type and a specific period of time.
 *

 * @apiHeader {String} Content-Type Media type of the resource.
 * @apiHeader {String} Authorization(Bearer token)  System login authentication token.
 * @apiHeader (Params) {String} [lang] API response language (es = Spanish , en = English)
 * 

 * @apiBody {Date}     [initial_date]       Consultation start date
 * @apiBody {Date}     [final_date]         End date of the query.
 * @apiBody {String}   [wallet]             Wallet registered in the application.
 * @apiBody {String}   [status]             Status of registered movements (in_process, complete).
 * @apiBody {Strings}  [pos]                Device ID linked to an address.
 * 

 * @apiSuccess {Boolean} success Indicates the status of the true or false response.
 * @apiSuccess {String} message Information on the user's transactions.
 * @apiSuccess {Object} transactions JSON with the response data.
 * @apiSuccess {Number} transactions.number_payments Indicates the number of payments.
 * @apiSuccess {Float} transactions.balance_payment Indicates the available balance in the wallet (USD).
 * @apiSuccess {Float} transactions.balance_pending Indicates the outstanding balance to be received in the wallet (USD).
 * @apiSuccess {Float} transactions.balance_payment_crypto Indicates the available balance in the wallet in cryptocurrency (BTC).
 * @apiSuccess {Float} transactions.balance_pending_cripto Indicates the outstanding balance in cryptocurrency to be received in the wallet (BTC).
 * @apiSuccess {Object} payments JSON with the detail of the payments.
 * @apiSuccess {String} payments.type_op Type of operation.
 * @apiSuccess {String} payments.invoice Indicate the invoice number if you have it.
 * @apiSuccess {String} payments.transactions_code Indicates the transaction code.
 * @apiSuccess {String} payments.address Indicates the address of the wallet.
 * @apiSuccess {String} payments.wallet Indicates the wallet.
 * @apiSuccess {String} payments.pos The ID of the device linked to an address.
 * @apiSuccess {Float} payments.usd_value Indicates the value of the transaction (USD).
 * @apiSuccess {String} payments.currency Indicates the cryptocurrency linked to the transaction (BTC).
 * @apiSuccess {String} payments.currency_payment Indicates the fiat currency linked to the transaction.
 * @apiSuccess {Float} payments.value Indicates the value of the transaction in cryptocurrency (BTC).
 * @apiSuccess {Float} payments.exchange_rate Indicates the exchange rate of the transaction (BTC-USD).
 * @apiSuccess {String} payments.status Indicates the status of the operation.
 * @apiSuccess {Date} payments.date Indicates the date and time of the operation.
 * @apiSuccess {String} payments.payment_type Indicate the type of payment.
 * 

 * @apiErrorExample Success 200 (example):
 *     {

    "success": true,
    "message": "User transaction information",
    "transactions": {
        "number_payments": 30,
        "balance_payment": "52,75",
        "balance_pending": "65,00",
        "balance_payment_crypto": "0,00188431",
        "balance_pending_cripto": "0,00103415",
        "payments": 

            {

                "type_op": "Pago Enviado",
                "invoice": "NO",
                "transactions_code": "POS-45582247ad39c84a64c50ad016ccc01d901152ec-828723397300968",
                "address": "n3tfQspHqp4uWH6t2zGwSDAFhcBKGwLUAa",
                "wallet": "marcostest01",
                "pos": "",
                "usd_value": "7,00",
                "currency": "BTC",
                "currency_payment": "USD",
                "value": "0,00011137",
                "exchange_rate": "62.853,55",
                "status": "COMPLETED",
                "date": "21/04/2022 03:06 pm",
                "payment_type": "Ecommerce"
            }



 * @apiErrorExample Error 400 (Example1):
 *     {

 *  "success": false,

 *  "message": "Error processing request, please try again later"

 *      }   

 *

 *

 * @apiErrorExample Error 503 (Example2):
 *     {

 *   "success": false,

 *   "message": "There is no information on payments made in the indicated wallet"

 *         }          

 *

 *  

 * @apiError (Error 400 Bad Request) {Boolean} success Request response status.
 * @apiError (Error 400 Bad Request) {String}  message Error message indicated by the request.
 *

 * @apiError (Error 503 Service Unavailable) {Boolean} success Request response status.
 * @apiError (Error 503 Service Unavailable) {String}  message Error message indicated by the request.
 * 

 */

function postUser() { return; }







/**

 * @api {POST} /send_payment_btc?lang=es  BTC sending

 * @apiVersion 0.0.1
 * @apiName Send BTC payments
 * @apiGroup Send BTC payments
 *

 * @apiDescription WEB service of type POST that allows to send payments in BTC.
 *

 * @apiHeader {String} Content-Type Media type of the resource.
 * @apiHeader {String} Authorization(Bearer token)  System login authentication token.
 * @apiHeader (Params){String} [lang] API response language (es = Spanish , en = English)
 * 

 * @apiBody {String} wallet Wallet registered in the application.
 * @apiBody {Float}  amount Amount of the payment request.
 * @apiBody {String} email  Email.
 * 

 * @apiSuccess {Boolean} success Indicates the status of the true or false response.
 * @apiSuccess {String} message Indicates whether the payment was sent.
 *

 * @apiErrorExample Success 200 (Example):
 *       {

 *   "success": false,

 *   "message": "The payment was sent successfully"

 *       }

 * 

 * @apiErrorExample Error 412 (Example):
 *       {

 *   "success": false,

 *   "message": "Error creating payment, please try again later"       

 *       }

 *

 * @apiErrorExample Error 500 (Example):
 *        {

 *   "success": false,

 *   "message": "Failed to get token please try again later"       

 *       }

 * 

 *  @apiErrorExample Error 503 (Example):
 *        {

 *   "success": false,

 *   "message": "Error processing request please try again later"       

 *       }

 *

*/



/**

 * @api {POST} /get_payment_qr?lang=es Generate payment URL by QR

 * @apiVersion 0.0.1
 * @apiName Generate payment by QR
 * @apiGroup Generate payment by QR
 * 

 * @apiDescription WEB service of type POST that allows to send payments in BTC.
 *

 * @apiHeader {String} Content-Type Media type of the resource.
 * @apiHeader {String} Authorization(Bearer token)  System login authentication token.
 * @apiHeader (Params){String} [lang] API response language (es = Spanish , en = English)
 * 

 * @apiBody {String} [wallet] Wallet registered in the application.
 * @apiBody {Float}  amount Amount of the payment request.
 * @apiBody {String} pos  ID del dispositivo vinculado a un address.
 * 

 * @apiSuccess {Boolean} success Indicates the status of the true or false response.
 * @apiSuccess {String} message Indicates whether the payment was sent.
 *

 * @apiErrorExample Success 200 (Example):
 *   

 *  {

    "success": true,

    "url": "127.0.0.1/dashboard/Api/pos/",

    "amount_btc": "0.00001000"

    }



   @apiErrorExample Error 412 (Example):

{

    "success": false,

    "message": "The indicated wallet does not correspond to the user"

}

   @apiErrorExample Error 412 (Example):

{

    "success": false,

    "message": "The indicated pos id does not correspond to the user"

}

    @apiErrorExample Error 503 (Example):

{

    "success": false,

    "message": "Error processing request, please try again later"

}

*/

/**

 * @api {POST} http://{deviceIP}:8085/sellQR  Send payment by QR

 * @apiVersion 0.0.1
 * @apiName Shipping by QR
 * @apiGroup Send by QR
 * 

 * @apiDescription WEB service of type POST that allows to send payments in BTC.
 * @apiParam {String} deviceIP Ip of the device.
 *

 * @apiHeader {String} Content-Type Media type of the resource.
 * @apiHeader {String} Authorization (Bearer token) Token associated with the IBLO Dashboard user, it is found in the Ecommerce → API route.
 * 

 * 

 * @apiBody {String} amount Amount of the payment request.
 * @apiBody {Float}  currency Indicates the type of currency.
 * @apiBody {String} pair Conversion currency to execute the transaction.
 * @apiBody {wallet} [wallet] Wallet in which you want to receive the payment.
 * 

 * @apiSuccess {String} success Indicates the status of the true or false response.
 * @apiSuccess {String} message Indicates whether the payment was sent.
 *

 *

*/


