#! /usr/bin/env php
<?php
(include_once __DIR__ . "/../vendor/autoload.php") or die("Composer not Found");


use GuzzleHttp\Client;

!defined("DS") && define("DS", DIRECTORY_SEPARATOR);

$http_client = new GuzzleHttp\Client;
$cli = new League\CLImate\CLImate;

$cli->addArt(__DIR__ . "/arts");
$cli->tab(5)->animation("robot")
  ->enterFrom("left");
$cli->inline("WELCOME");

function getDirectory($message, $type = "dir")
{
  global $cli;
  $input = $cli->br()->green()->out('<blue>NOTE:</blue> CWD=' . getcwd());
  $input = $cli->input($message);

  function_exists($func = "is_" . $type) || $func = "is_dir";
  $input->accept(function ($response) use ($cli, $func) {
    $response = str_replace("~", $_SERVER["HOME"], $response);
    if (!$func($response)) {
      $cli->error("ERROR: you must enter a valid directory!!");
      return false;
    }

    return true;
  });

  return $input->prompt();
}

function checkDir($dir)
{
  if (!is_file($dir . DS . "apidoc.json")) {
    $directory = getDirectory(join("\n", [
      "<red>Not found apidoc.json</red>",
      "1 - Please you apidoc root dir:"
    ]));
    if (checkDir($directory) && chdir($directory)) return $directory;
  } else return $dir;
}


function getTranslation($text, $from = "es", $to = "en", $provider = "microsoft")
{

  $response = (new Client)->request('POST', "https://webmail.smartlinkcorp.com/dotrans.php", [
    "form_params" => [
      "dir" => join("/", [$from, $to]),
      "provider" => $provider,
      "text" => $text
    ]
  ]);

  if ($response->getStatusCode() != 200)
    throw new Exception(
      "Ha ocurrido un error al realizar la peticon \n ERROR: " . $response->getStatusCode(),
      1
    );
  $body = (string)$response->getBody();
  return $body;
};

$apidoc_dir = checkDir(getcwd());

$cli->br()
  ->green()
  ->out('The directory has been detected: ' . $apidoc_dir);

$src_dir = getDirectory("2 - Please enter the resources dir <blue>DEFAULT \"$apidoc_dir\": </blue>");

$cli->br()
  ->green()
  ->out('The directory has been detected: ' . $src_dir);


function getAllFiles($dir)
{
  return array_reduce(scandir($dir), function ($allFiles, $res) use ($dir) {

    if (in_array($res, [".", ".."])) return $allFiles;
    $res = $dir . DS . $res;
    //$res = $dir . DS . $res;
    if (is_dir($res)) {
      return array_merge($allFiles, getAllFiles($res));
    };

    if (is_file($res))
      $allFiles[] = $res;

    return $allFiles;
  }, []);
}





$files_contents = array_map(
  fn ($file) => [$file, file($file)],
  getAllFiles($src_dir)
);

$progress = $cli->progress()->total(count($files_contents));
$files_modifieds = array_map(function ($file) use ($progress) {

  /* $cli->backgroundMagenta() */
  /*   ->out(join("", [ */
  /*     "<bold> <green>[[ INFO ]] </red> </green>", */
  /*     "Modificando: ", */
  /*     "<bold>" . $file[0] . " </bold>" */
  /*   ])); */
  $x = explode(DS, $file[0]);
  $x[0] = $x[0] . "_en";
  $new_file = join(DS, $x);

  if (file_exists($new_file)) unlink($new_file);
  else if (!file_exists(dirname($new_file)))
    mkdir(dirname($new_file), 0777, TRUE);

  if (is_array($file[1])) $translates_lines = join("\n", array_map(function ($line) {
    if (preg_match("/( *\* +)@(api\w+)(.+)/", $line, $match)) {
      //var_dump($line);
      $prefix = $match[1];
      $func = $match[2];
      $params = trim($match[3]);
      if (isset($func)) {

        switch ($func) {
          case "api":
          case "apiParam":
          case "apiSuccess":
            if (!preg_match("/^(\{\w+\})?\ +(\[?[A-Za-z0-9:?=&\[\]\.\_\-]+\]?)\ +(.*)/", $params, $match2)) break;
            $params_list = array_slice($match2, 1);
            $params_list[2] = getTranslation(html_entity_decode($params_list[2]));
            $params = join(" ", $params_list);
            break;
          case "apiDescription":
          case "apiDefine":
          case "apiName":
            $params = getTranslation($params);
            break;
        }

        $line = $prefix . '@' . $func . " " . $params;

        /* $cli->backgroundMagenta() */
        /*   ->out(join("", [ */
        /*     "<bold> <green>[[ INFO ]] </red> </green>", */
        /*     "Modificando: ", */
        /*     "<bold>" . $line . " </bold>" */
        /*   ])); */
      }
    }
    return $line;
  }, $file[1]));

  file_put_contents($new_file, $translates_lines, FILE_APPEND);
  $progress->advance();
  return $translates_lines;
}, $files_contents);

$cli->dump(serialize($files_modifieds));
