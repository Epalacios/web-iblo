/**
 * @api {POST} /get_token_auth?lang=es&/ Obtener Token
 * @apiVersion 0.0.1
 * @apiName GetToken
 * @apiGroup Token
 * 
 * @apiDescription Servicio web Rest Full de tipo POST, permite obtener un token de autorización.
 * @apiHeader {String} Content-Type Tipo de medio del recurso.
 * @apiHeader (Params) {String} [lang] Idioma de respuesta del API (es = español , en = inglés).
 * 
 * @apiBody {String}   username     Usuario autorizado a la aplicación. 
 * @apiBody {String}   password     Contraseña registrada en la aplicación.
 * 
 * 
 * @apiSuccess {Boolean}  success     Indica el estado de la respuesta true or false.
 * @apiSuccess {String}   token        codigo alfanúmerico que corresponde al token de autorización en la aplicación.
 *
 * @apiError (Error 401 Unauthorized) {Boolean} success Estatus de respuesta de la petición.
 * @apiError (Error 401 Unauthorized) {String}  message Mensaje de error indicado por la petición.
 * 
 * 
 * @apiError (Error 503 Service Unavailable) {Boolean} success Estatus de respuesta de la petición.
 * @apiError (Error 503 Service Unavailable) {String}  message Mensaje de error indicado por la petición.
 * 
 * @apiErrorExample Success 200 (Ejemplo):
 *     {
 *   "success": true,
 *   "token": "oyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpZCI6IjI0IiwidXNlcm5hbWUiOiJtYXJjb3N0ZXN0MDEiLCJpYXQiOjE2NTIwMTk3MDUsImV4cCI6MTY4MzEyMzcwNX0.5DFWtdBi_lSW3VaEzWDOiKs8vkn6rIXEaRyJLay59PU"
 *   }
 * @apiErrorExample Error 401 (Ejemplo1):
 *    {
 *  "success": false,
 *  "message": "Usuario/Contraseña inválidos"
 *   }
 * @apiErrorExample Error 401 (Ejemplo2):
 *    {
 *   "success": false,
 *    "message": "Cuenta Inactiva, por favor revise su correo y active su cuenta"
 *   }
 * @apiErrorExample Error 503 (Ejemplo3):
 *    {
 *   "success": false,
 *   "message": "Error al procesar la petición, por favor intente mas tarde"
 *   }
 * 
 */
function getUser() { return; }
/**
 * @api {post} /get_payments_pos?lang=es Listar Pagos
 * @apiVersion 0.0.1
 * @apiName  Pagos
 * @apiGroup Listado de pagos
 
 *
 * @apiDescription Servicio Web que permite listar los pagos por tipo y un período de tiempo específico. 
 *
 * @apiHeader {String} Content-Type Tipo de medio del recurso.
 * @apiHeader {String} Authorization(Bearer token)  Token de autentificación de inicio de sesión del sistema. 
 * @apiHeader (Params) {String} [lang] Idioma de respuesta del API (es = español , en = inglés)
 * 
 * @apiBody {Date}     [initial_date]       Fecha de inicio de la consulta
 * @apiBody {Date}     [final_date]         Fecha final de la consulta.
 * @apiBody {String}   [wallet]             Billetera registrada en la aplicación.
 * @apiBody {String}   [status]             Estatus de los movimientos registrados (in_process, complete).
 * @apiBody {Strings}  [pos]                ID del dispositivo vinculado a un address.
 * 
 * @apiSuccess {Boolean} success         Indica el estado de la respuesta true or false.
 * @apiSuccess {String} message         Información de las transacciones del usuario.
 * @apiSuccess {Object} transactions    JSON con los datos de la respuesta.
 * @apiSuccess {Number}     transactions.number_payments        Indica el número de pagos.
 * @apiSuccess {Float}      transactions.balance_payment        Indica el saldo disponible en la wallet (USD) .
 * @apiSuccess {Float}      transactions.balance_pending        Indica el saldo pendiente por recibir en la wallet (USD).
 * @apiSuccess {Float}      transactions.balance_payment_crypto        Indica el saldo disponible en la wallet en criptomoneda (BTC) .
 * @apiSuccess {Float}      transactions.balance_pending_cripto        Indica el saldo pendiente en criptomoneda por recibir en la wallet (BTC).
 * @apiSuccess {Object} payments    JSON con el detalle de los pagos.
 * @apiSuccess {String}     payments.type_op    Tipo de operación.
 * @apiSuccess {String}     payments.invoice    Indica el número de factura en caso de poseerla.
 * @apiSuccess {String}     payments.transactions_code    Indica el código de la transacción.
 * @apiSuccess {String}     payments.address    Indica la dirección de la billetera.
 * @apiSuccess {String}     payments.wallet     Indica la billetera.
 * @apiSuccess {String}     payments.pos        ID del dispositivo vinculado a un address.
 * @apiSuccess {Float}      payments.usd_value   Indica el valor de la transacción (USD).
 * @apiSuccess {String}     payments.currency     Indica la criptomoneda vinculada a la transacción (BTC).
 * @apiSuccess {String}     payments.currency_payment    Indica la moneda fiat vinculada a la transacción .
 * @apiSuccess {Float}      payments.value    Indica el valor de la transacción en criptomoneda (BTC).
 * @apiSuccess {Float}      payments.exchange_rate    Indica la tasa de cambio de la transacción (BTC-USD).
 * @apiSuccess {String}     payments.status    Indica el estatus de la operación.
 * @apiSuccess {Date}       payments.date    Indica la fecha y hora de la operación.
 * @apiSuccess {String}     payments.payment_type    Indica el tipo de pago.
 * 
 * @apiErrorExample Success 200 (example):
 *     {
    "success": true,
    "message": "Información de las transacciones del usuario",
    "transactions": {
        "number_payments": 30,
        "balance_payment": "52,75",
        "balance_pending": "65,00",
        "balance_payment_crypto": "0,00188431",
        "balance_pending_cripto": "0,00103415",
        "payments": [
            {
                "type_op": "Pago Enviado",
                "invoice": "NO",
                "transactions_code": "POS-45582247ad39c84a64c50ad016ccc01d901152ec-828723397300968",
                "address": "n3tfQspHqp4uWH6t2zGwSDAFhcBKGwLUAa",
                "wallet": "marcostest01",
                "pos": "",
                "usd_value": "7,00",
                "currency": "BTC",
                "currency_payment": "USD",
                "value": "0,00011137",
                "exchange_rate": "62.853,55",
                "status": "COMPLETED",
                "date": "21/04/2022 03:06 pm",
                "payment_type": "Ecommerce"
            }

 * @apiErrorExample Error 400 (Ejemplo1):
 *     {
 *  "success": false,
 *  "message": "Error al procesar la petición, por favor intente mas tarde"
 *      }   
 *
 *
 * @apiErrorExample Error 503 (Ejemplo2):
 *     {
 *   "success": false,
 *   "message": "No se tiene información de pagos realizados en la wallet indicada"
 *         }          
 *
 *  
 * @apiError (Error 400 Bad Request) {Boolean} success Estatus de respuesta de la petición.
 * @apiError (Error 400 Bad Request) {String}  message Mensaje de error indicado por la petición.           
 *
 * @apiError (Error 503 Service Unavailable) {Boolean} success Estatus de respuesta de la petición.
 * @apiError (Error 503 Service Unavailable) {String}  message Mensaje de error indicado por la petición.         
 * 
 */
function postUser() { return; }



/**
 * @api {POST} /send_payment_btc?lang=es  Envio de BTC
 * @apiVersion 0.0.1
 * @apiName Enviar pagos BTC
 * @apiGroup Enviar pagos BTC
 *
 * @apiDescription Servicio Web de tipo POST que permite enviar pagos en BTC.
 *
 * @apiHeader {String} Content-Type Tipo de medio del recurso.
 * @apiHeader {String} Authorization(Bearer token)  Token de autentificación de inicio de sesión del sistema. 
 * @apiHeader (Params){String} [lang] Idioma de respuesta del API (es = español , en = inglés)
 * 
 * @apiBody {String} wallet Billetera registrada en la aplicación.
 * @apiBody {Float}  amount Monto de la solicitud de pago.
 * @apiBody {String} email  Correo electrónico.
 * 
 * @apiSuccess {Boolean} success         Indica el estado de la respuesta true or false.
 * @apiSuccess {String} message         Indica si el pago fue enviado.
 *
 * @apiErrorExample Success 200 (Ejemplo):
 *       {
 *   "success": false,
 *   "message": "El pago fue enviado satisfactoriamente"
 *       }
 * 
 * @apiErrorExample  Error 412 (Ejemplo):   
 *       {
 *   "success": false,
 *   "message": "Error al crear el pago, por favor intente mas tarde"       
 *       }
 *
 * @apiErrorExample  Error 500 (Ejemplo):   
 *        {
 *   "success": false,
 *   "message": "Error al obtener el token por favor intente mas tarde"       
 *       }
 * 
 *  @apiErrorExample Error 503 (Ejemplo): 
 *        {
 *   "success": false,
 *   "message": "Error al procesar la petición por favor intente mas tarde"       
 *       }
 *
*/

/**
 * @api {POST} /get_payment_qr?lang=es Generar URL de pago por QR
 * @apiVersion 0.0.1
 * @apiName Generar pago por QR 
 * @apiGroup Generar pago por QR 
 * 
 * @apiDescription Servicio Web de tipo POST que permite enviar pagos en BTC.
 *
 * @apiHeader {String} Content-Type Tipo de medio del recurso.
 * @apiHeader {String} Authorization(Bearer token)  Token de autentificación de inicio de sesión del sistema. 
 * @apiHeader (Params){String} [lang] Idioma de respuesta del API (es = español , en = inglés)
 * 
 * @apiBody {String} [wallet] Billetera registrada en la aplicación.
 * @apiBody {Float}  amount Monto de la solicitud de pago.
 * @apiBody {String} pos  ID del dispositivo vinculado a un address.
 * 
 * @apiSuccess {Boolean} success         Indica el estado de la respuesta true or false.
 * @apiSuccess {String} message         Indica si el pago fue enviado.
 *
 * @apiErrorExample Success 200 (Ejemplo):
 *   
 *  {
    "success": true,
    "url": "127.0.0.1/dashboard/Api/pos/",
    "amount_btc": "0.00001000"
    }

   @apiErrorExample Error 412 (Ejemplo):
{
    "success": false,
    "message": "La wallet indicada no corresponde al usuario"
}
   @apiErrorExample Error 412 (Ejemplo):
{
    "success": false,
    "message": "El id de pos indicado no corresponde al usuario"
}
    @apiErrorExample Error 503 (Ejemplo):
{
    "success": false,
    "message": "Error al procesar la petición, por favor intente mas tarde"
}
*/
/**
 * @api {POST} http://{deviceIP}:8085/sellQR  Enviar pago por QR
 * @apiVersion 0.0.1
 * @apiName Envio por QR
 * @apiGroup Envio por QR
 * 
 * @apiDescription Servicio Web de tipo POST que permite enviar pagos en BTC.
 * @apiParam {String} deviceIP IP del dispositivo.
 *
 * @apiHeader {String} Content-Type Tipo de medio del recurso.
 * @apiHeader {String} Authorization (Bearer token) Token asociado al usuario de IBLO Dashboard, el mismo se encuentra en la ruta Ecommerce → API.
 * 
 * 
 * @apiBody {String} amount Monto de la solicitud de pago.
 * @apiBody {Float}  currency Indica el tipo de moneda.
 * @apiBody {String} pair Moneda de conversión para ejecutar la transacción.
 * @apiBody {wallet} [wallet] Biletera en la cual se desea recibir el pago .
 * 
 * @apiSuccess {String} success         Indica el estado de la respuesta true or false.
 * @apiSuccess {String} message         Indica si el pago fue enviado.
 *
 *
*/

