## Módulo de Registro de nuevo usuario

Para realizar el registro completa los datos solicitados: Nombre del negocio, Nombre de contacto, Nombre de usuario, Correo Electrónico y una contraseña en el formato permitido por la aplicación.

**Formato de contraseña: Mínimo 8 caracteres, mínimo una mayúscula, mínimo una minúscula, mínimo un número y mínimo un  carácter especial (!@#$%&*).**

<center>
![Menú Actualizaciones](/img/nuevo_usuario.png "Menú Actualizaciones")
</center>

Una vez ingresado todos los datos solicitados y estar de acuerdo con las Políticas de Privacidad, los Términos y las condiciones, seleccionar el botón Registrar Usuario.

Seguidamente, la Plataforma Iblo Dashboard envía un mensaje al correo electrónico registrado previamente para activar la cuenta.

<center>
![Menú Actualizaciones](/img/correo.png "Menú Actualizaciones")
</center>

Al presionar el botón Activar Cuenta ya su cuenta estará activa y podrá acceder a Iblo Dashboard.