#  Módulo de Dispositivos

<center>
![Menú Actualizaciones](/img/menu_dispositivo.png "Menú Actualizaciones")
</center>

#  Módulo de POS

Lista los datos de las billeteras registradas en la plataforma Iblo además lista todas las direcciones address registradas a Iblo y cuáles de ella están asociadas a un dispositivo POS (puntos de ventas).

<center>
![Menú Actualizaciones](/img/dispositivo1.png "Menú Actualizaciones")
</center>


Al situar el puntero del mouse encima de botón verde se visualizara este mensaje.

<center>
![Menú Actualizaciones](/img/dispositivo2.png "Menú Actualizaciones")
</center>

Si el usuario desea desvincular el dispotivo vinculado solo debera precionar el botón verde.

Recuerde que al ingresar de nuevo a la aplicación Iblo Dashboard en su telefono , su dispositivo se encontrará desvinculado. Si desea ingresar deberá vincular el dispositivo de nuevo.

Al situar el puntero del mouse encima del icono de dispostivo se visualizará este mensaje, el mismo especifica el modelo de dispotivo vinculado.

<center>
![Menú Actualizaciones](/img/dispositivo3.png "Menú Actualizaciones")
</center>


