# Módulo de Dashboard

Al ingresar al sistema del lado izquierdo puede visualizar un menú. El mismo se desplega de forma dinámica, es decir de acuerdo a la permisología asignada para cada usuario.

<center>
![Menú](/img/menu.png "Menú")
</center>

Este módulo detalla de manera resumida los datos más relevantes, (Mis pagos, Pagos completados, Pagos pendientes, Monto pagos por mes, Cantidad de pagos por mes). 

En la siguiente pantalla se visualiza el monto recibido en dolares y BTC ademas el monto pendiente por recibir en dolares y el monto pendiente por recibir en BTC.

<center>
![Menú](/img/mispagos.png "Menú")
</center>

En la siguiente pantalla se visualiza el numero de pagos completados, el monto total recibido en dolares y en BTC.

<center>
![Menú](/img/pagos_completados.png "Menú")
</center>

En la siguiente pantalla se especifica la cantidad de pagos pendientes, el dinero recibido en dolares y BTC de dichos pagos pendientes.

<center>
![Menú](/img/pagos_pendientes.png "Menú")
</center>

En la siguiente pantalla se visualiza un gráfico especificando el monto recibido por mes.  

<center>
![Menú](/img/grafica_pagos.png "Menú")
</center>

En la siguiente pantalla se visualiza un gráfico de barras especificando la cantidad de pagos recibidos por mes.  

<center>
![Menú](/img/grafica_pagos2.png "Menú")
</center>

Seguidamente esta pantalla especifica las últimas 5 transacciones recibidas visualizando en donde se puede ver el progreso y estatus de cada una de ellas.

<center>
![Menú](/img/inicio_2.png "Menú")
</center>