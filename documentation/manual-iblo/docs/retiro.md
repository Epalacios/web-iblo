#  Módulo de Retiro

Este modulo se encarga de retiros de Saldo de la Billetera en la plataforma de Iblo Dashboard.

Nota: Cabe destacar que la Wallet de BTC de envio debe ser una wallet de BTC de Trezor. 

Para realizar retiros de Saldo de la Billetera en la plataforma de Iblo Dashboard se deben seguir los siguientes pasos:

Paso 1: Seleccionar la Wallet BTC de envío (solo disponible por trezor).

<center>
![Menú Actualizaciones](/img/retiro.png "Menú Actualizaciones")
</center>

Paso 2: Ingresar una wallet de retiro valida.

<center>
![Menú Actualizaciones](/img/retiro.png "Menú Actualizaciones")
</center>

Paso 3: Ingresar el monto del retiro.

Si desea retirar todos sus fondos puede clicquear el boton MAX

<center>
![Menú Actualizaciones](/img/retiro.png "Menú Actualizaciones")
</center>

Paso 4 : Cliquear el botón retiro.

<center>
![Menú Actualizaciones](/img/retiro.png "Menú Actualizaciones")
</center>

Finalmente, la aplicación redireccionará al portal de Trezor en donde deberá seguir los pasos de dicha aplicación para finalizar el retiro.

Nota: esta última aplicación es externa a Iblo y cualquier detalle u error no depende de nuestra aplicación.

<center>
![Menú Actualizaciones](/img/trezor.png "Menú Actualizaciones")
</center>