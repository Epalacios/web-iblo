#  Módulo de E-commerce

<center>
![Menú Actualizaciones](/img/menu_ecommerce.png "Menú Actualizaciones")
</center>


#  Módulo de API
Este módulo permite al usuario cambiar las rutas de redirección al momento de utilizar la pasarela de pago BTC (pasarela que se genera cuando el usuario paga por SMS, correo electrónico y el botón de pago.)

Además permite copiar el token para integrar con otros productos de Iblo



<center>
![Menú Actualizaciones](/img/ecommerce.png "Menú Actualizaciones")
</center>

Se solicita que especifiqué la URL en caso de error e indiqué la URL en caso de éxito.

#  Módulo de Widget

Este módulo nos permite generar un botón de pago que puede ser implementado en páginas externas a Iblo Dasboard.
 
Para generar un botón de pago con la plataforma Iblo se debe seguir los siguientes pasos:

Paso 1: Seleccionar la billetera.

<center>
![Menú Actualizaciones](/img/paso1.png "Menú Actualizaciones")
</center>

Paso 2: Seleccionar el Tipo de Botón (Precio variable o Precio Fijo). 

En caso de generar un botón de pago (Precio variable).

<center>
![Menú Actualizaciones](/img/paso2.png "Menú Actualizaciones")
</center>

El paso 3 será: Darle clic Generar botón.

<center>
![Menú Actualizaciones](/img/paso3.png "Menú Actualizaciones")
</center>

Para finalmente tener el botón de pago con la plataforma Iblo.


<center>
![Menú Actualizaciones](/img/pantalla_final.png "Menú Actualizaciones")
</center>

Para que el pago sea variable debe cambiar el valor del campo oculto que tiene el valor de id ="smartbutton-cryptoesb-amount". El valor por defecto de dicho campo es 1 y según el valor que tenga se generará el monto de pago equivalente en BTC. 


<center>
![Menú Actualizaciones](/img/pantalla_final_2.png "Menú Actualizaciones")
</center>

En caso de generar un botón de pago (Precio Fijo).

El paso 3 será: Ingresar el monto.

<center>
![Menú Actualizaciones](/img/paso3_1.png "Menú Actualizaciones")
</center>

El paso 4 será: Darle clic en Generar Botón.

<center>
![Menú Actualizaciones](/img/paso4.png "Menú Actualizaciones")
</center>

Para finalmente tener el botón de pago con la plataforma Iblo.

<center>
![Menú Actualizaciones](/img/pantalla_final_1.png "Menú Actualizaciones")
</center>


Al dar clic al botón copiar código permitirá obtener el código HTML, el cual podrá ser implementado en cualquier página de HTML estándar.