# Módulo de Ingreso al Sistema
Para acceder a la plataforma Iblo Dashboard, ingrese su Usuario o Correo Electrónico y su Contraseña.
<center>
![Menú Actualizaciones](/img/login.png "Menú Actualizaciones")
</center>

Una vez ingresado su usuario y su contraseña, la plataforma verifica sus datos.

<center>
![Menú Actualizaciones](/img/revision.png "Menú Actualizaciones")
</center>

Si sus datos son correctos, la plataforma le da el acceso al sistema.

Si el usuario es nuevo en la plataforma de Iblo Dashboard se visualizará la siguiente pantalla, especificando que el usuario no posee wallet asociada.

<center>
![Menú Actualizaciones](/img/wallet.png "Menú Actualizaciones")
</center>

Al dar clic en el botón Importar Wallet nos redirecciona a la sección Billetera (Mis llaves públicas) donde se importará la wallet del usuario.  