#  Módulo de Ajustes

<center>
![Menú Actualizaciones](/img/ajustes.png "Menú Actualizaciones")
</center>

Esta configuración permite seleccionar cuantas confirmaciones debe esperar el servicio de validación de pago de BTC para poder cambiar un estatus de un pago a pago completado.

<center>
![Menú Actualizaciones](/img/ajustes2.png "Menú Actualizaciones")
</center>