#  Módulo de Billeteras

Este modulo nos permite visualizar las wallet importadas al usuario y ademas importar wallets aceptadas (Wallet Trezor, Wallet Legger, y otras walles no custodiables).

<center>
![Menú Actualizaciones](/img/menu_billetera.png "Menú Actualizaciones")
</center>

## Módulo de Mis Llaves Públicas

A continuación el usuario deberá ingresar los datos solicitados para importar su wallet

<center>
![Menú Actualizaciones](/img/billetera1.png "Menú Actualizaciones")
</center>

Una vez ingresado los datos, el usuario deberá dar clic en el botón importar 

Si la importación se realizó exitosamente, los datos aparecerán en la pantalla Información de Wallets de Usuario

Las llaves publicas tienen ser xpub.

**Nota: La plataforma Iblo Dasboard no guarda llaves privadas.**

<center>
![Menú Actualizaciones](/img/billetera2.png "Menú Actualizaciones")
</center>

al dar clic al icono de la seccion acciones , desencripta la llave pública vinculada a la wallet.

<center>
![Menú Actualizaciones](/img/billetera3.png "Menú Actualizaciones")
</center>