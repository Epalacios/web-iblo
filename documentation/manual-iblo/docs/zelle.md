#  Módulo de Zelle

<center>
![Menú Actualizaciones](/img/menu_zelle.png "Menú Actualizaciones")
</center>

## Módulo Registro Zelle

Este modulo solicita un registro de correo Zelle el cual será utilizado para validar los pagos realizados a través de este medio en una pasarela virtual.

Para realizar el registro de correo Zelle deberá ingresar el correo electrónico, la contraseña y además una etiqueta para identificar fácilmente el correo.

<center>
![Menú Actualizaciones](/img/zelle1.png "Menú Actualizaciones")
</center>


## Módulo Mis Pagos en Zelle

A continuación se visualiza el Historial de Pagos recibidos en el correo de Zelle registrado previamente, los cuales fueron validados en la pasarela virtual, visualizando datos de la operación tales como: El número de referencia, la descripción, el monto, el monto total, el ID de la factura, el estatus y la fecha de operación. 

<center>
![Menú Actualizaciones](/img/zelle2.png "Menú Actualizaciones")
</center>
