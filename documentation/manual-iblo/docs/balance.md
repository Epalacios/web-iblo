#  Módulo de Balance

Este módulo sé índica el saldo disponible en cada Wallet asociada a la plataforma Iblo Dashboard.

Además, lista el conjunto de operaciones realizadas en un período de tiempo determinado, muestra el balance de la Wallet seleccionada.

<center>
![Menú Actualizaciones](/img/blance.png "Menú Actualizaciones")
</center>

Al situar el puntero del mouse encima de icono del apartado ver detalle y cliquear nos muestra una pantalla un detalle de la operación.

Indicando el ID de la operación, el Hash, el número de bloque, el monto pagado expresado en dólares y el cambio a BTC, la tasa de cambio, la fecha y estatus de la operación.

<center>
![Menú Actualizaciones](/img/balance5.png "Menú Actualizaciones")
</center>

Esta sección nos permite listar la información de las operaciones por número de filas.

<center>
![Menú Actualizaciones](/img/balance4.png "Menú Actualizaciones")
</center>

Esta sección nos permite listar las columnas específicas que necesita visualizar el usuario.

<center>
![Menú Actualizaciones](/img/balance3.png "Menú Actualizaciones")
</center>

Esta seccion nos permite obtener informacion mediante un buscador el cual filtrara la información por cualquiera de las columnas.

<center>
![Menú Actualizaciones](/img/balance6.png "Menú Actualizaciones")
</center>

Además, podemos al dar clic en la sección copiar,  se copian todos los datos de las operaciones realizadas en el portapapeles del usuario.

Finalmente, al dar clic en la sección exportar PDF se descarga en un PDF todos los datos de las operaciones realizadas.

