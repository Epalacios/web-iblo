#  Módulo de Mi Perfil
<center>
![Menú Actualizaciones](/img/menu_perfil.png "Menú Actualizaciones")
</center>

## Módulo de Usuario

Permite actualizar los datos previamente registrados tales como: El nombre de su negocio y El nombre de contacto.

Nota: Para realizar actualizaciones en los datos es necesario colocar la contraseña. En caso de que sea una contraseña distinta a la que se tiene actualmente la misma se cambiará

<center>
![Menú Actualizaciones](/img/usuario.png "Menú Actualizaciones")
</center>


En esta pantalla se visualiza datos como: Correo Electrónico, Nombre de la Wallet, Nombre de usuario, y especifica el tipo de plan que posee el usuario.

Además, nos permite subir una foto perfil de usuario. 

<center>
![Menú Actualizaciones](/img/usuario2.png "Menú Actualizaciones")
</center>

Para subir una foto de perfil de usuario solo se debe cliquear encima de la imagen de usuario. 

**Nota: El formato para la foto de perfil es 400x100 píxeles.**

## Módulo de Suscripción

A continuación se visualiza el tipo de plan que posee el usuario especificando las características de dicho plan.

<center>
![Menú Actualizaciones](/img/suscripcion.png "Menú Actualizaciones")
</center>

En esta sección se visualiza el Historial de pagos, donde podemos encontrar la suscripción que adquirió, tipo de pago de la operación, el monto de la operación, la fecha de pago de la operación, el número de referencia y el estatus de la operación.

<center>
![Menú Actualizaciones](/img/suscripcion2.png "Menú Actualizaciones")
</center>