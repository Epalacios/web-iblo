# Módulo de Correo electrónico.

<center>
![Menú Actualizaciones](/img/correo_electronico.jpeg "Menú Actualizaciones")
</center>

# Módulo de Pago por correo electrónico.

Este módulo se encarga de generar un pago de BTC mediante correo electrónico.

Para realizar un pago debemos ingresar el correo electrónico y el monto en dólares o en BTC.

<center>
![Menú Actualizaciones](/img/correo_electronico1.jpeg "Menú Actualizaciones")
</center>

Una vez ingresado el correo electrónico y el monto deberá dar clic en enviar, la aplicación móvil de Iblo mostrará la siguiente pantalla solicitando una confirmación.

Si el usuario está de acuerdo con la solicitud de pago deberá dar clic en enviar

<center>
![Menú Actualizaciones](/img/correo_electronico4.jpeg "Menú Actualizaciones")
</center>

Seguidamente, la aplicación enviará un mensaje al correo electrónico indicado previamente.

<center>
![Menú Actualizaciones](/img/correo_electronico3.jpeg "Menú Actualizaciones")
</center>

Si el correo fue enviado satisfactoriamente, se mostrará este mensaje.

<center>
![Menú Actualizaciones](/img/correo_electronico2.jpeg "Menú Actualizaciones")
</center>

El usuario deberá ingresar al correo indicado previamente y encontrará un correo de Iblo con la solicitud de pago.

<center>
![Menú Actualizaciones](/img/correo_electronico5.png "Menú Actualizaciones")
</center>

Deber dar clic en el botón Pagar, una vez presione el botón se enviará a la pasarela de pago de Iblo.

<center>
![Menú Actualizaciones](/img/correo_electronico6.png "Menú Actualizaciones")
</center>

Se tendrá un plazo de 30:00 minutos para realizar el pago.

Una vez verificado el pago, se mostrará este mensaje.

<center>
![Menú Actualizaciones](/img/correo_electronico7.png "Menú Actualizaciones")
</center>