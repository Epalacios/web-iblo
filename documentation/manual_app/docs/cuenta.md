## Modulo de cuenta

<center>
![Menú Actualizaciones](/img/cuenta.jpeg "Menú Actualizaciones")
</center>

Al dar clic en el módulo de cuenta se visualizará un menú especificando datos del usuario tales como: (Correo electrónico, Wallet Actual, ID del Dispositivo). Además se encuentran tres módulos (Recuperar tu contraseña, Subscripción y Cerrar sesión).

<center>
![Menú Actualizaciones](/img/menu_derecho.jpeg "Menú Actualizaciones")
</center>

### Modulo de recuperar tu cuenta

El módulo de recuperar tu cuenta permite cambiar su contraseña en caso de olvidarla.

Deberá ingresar el correo electrónico asociado a la cuenta de iblo.

<center>
![Menú Actualizaciones](/img/recuperar.jpeg "Menú Actualizaciones")
</center>

Seguidamente la aplicación enviará a su correo electrónico un link para el cambio de su contraseña.

<center>
![Menú Actualizaciones](/img/recuperar1.png "Menú Actualizaciones")
</center>

Deberá ingresar su nueva contraseña de acceso a la aplicación. Esta contraseña deberá cumplir las siguientes validaciones.

**Formato de contraseña: Mínimo 8 caracteres, mínimo una mayúscula, mínimo una minúscula, mínimo un número y mínimo un  carácter especial (!@#$%&*).**


<center>
![Menú Actualizaciones](/img/recuperar2.png "Menú Actualizaciones")
</center>

### Modulo de Subscripción

El módulo de subscripción nos muestra los planes disponibles y los limites de operación a un usuario.

Para realizar la subscripción deberá presionar el botón Subscribirse

<center>
![Menú Actualizaciones](/img/plan2.jpeg "Menú Actualizaciones")
</center>

Deberá indicar su método de pago (PayPal, Tarjeta de Débito o Crédito)

<center>
![Menú Actualizaciones](/img/plan3.jpeg "Menú Actualizaciones")
</center>

En caso de haber indicado Paypal deberá ingresar sus credenciales de acceso a la plataforma.

<center>
![Menú Actualizaciones](/img/plan4.jpeg "Menú Actualizaciones")
</center>

Deberá elegir su forma de pago.

<center>
![Menú Actualizaciones](/img/plan6.jpeg "Menú Actualizaciones")
</center>

Seguidamente se mostrá una pantalla de confirmación del pago.

<center>
![Menú Actualizaciones](/img/plan5.jpeg "Menú Actualizaciones")
</center>

Deberá esperar unos segundos

<center>
![Menú Actualizaciones](/img/plan7.jpeg "Menú Actualizaciones")
</center>

Una vez confirmado el pago el usuario se subscribirá al plan seleccionado.

<center>
![Menú Actualizaciones](/img/plan8.jpeg "Menú Actualizaciones")
</center>

Para finalizar se mostrará su plan activo.

<center>
![Menú Actualizaciones](/img/plan1.jpeg "Menú Actualizaciones")
</center>

En caso de haber indicado Tarjeta de Débito o Crédito deberá ingresar todos los datos solicitados para realizar la operación.

<center>
![Menú Actualizaciones](/img/tarjeta.jpeg "Menú Actualizaciones")
</center>