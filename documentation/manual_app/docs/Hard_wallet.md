# Módulo de Hard Wallet
<center>
![Menú Actualizaciones](/img/hard_wallet.jpeg "Menú Actualizaciones")
</center>

En caso de que al iniciar seccion en la aplicacion de Iblo App y no haber creado una wallet, ya que tenia una Wallet previamente importada a traves de Iblo Dashboard, esta funcionalidad le permitirá crear una Hard Wallet 

Para crear una Wallet desde la aplicación deberá dar clic en el botón Crear

<center>
![Menú Actualizaciones](/img/importar_wallet.jpeg "Menú Actualizaciones")
</center>

La aplicación muestra una pantalla confirmando si desea crear una nueva Wallet, deberá dar clic en el botón Continuar.

<center>
![Menú Actualizaciones](/img/importar_wallet2.jpeg "Menú Actualizaciones")
</center>

El paso 1 se encarga de generar las palabras semillas deberá guardarlas en el orden especificado para continuar con el siguiente paso deberá dar clic en el botón Continuar,

<center>
![Menú Actualizaciones](/img/importar_wallet3.jpeg "Menú Actualizaciones")
</center>

La aplicación envia una pantalla de confirmación deberá dar clic al botón Continuar

<center>
![Menú Actualizaciones](/img/importar_wallet4.jpeg "Menú Actualizaciones")
</center>

El paso 2 se encarga de confirmar las frases semillas en el orden especifico dado en el paso anterior.

<center>
![Menú Actualizaciones](/img/importar_wallet5.jpeg "Menú Actualizaciones")
</center>

El paso 3 se encarga de solicitar el nombre de etiqueta de la Wallet y la contraseña.

<center>
![Menú Actualizaciones](/img/importar_wallet6.jpeg "Menú Actualizaciones")
</center>

Al dar clic en botón Crear Wallet deberá esperar unos segundos para la creación de la Wallet

<center>
![Menú Actualizaciones](/img/importar_wallet7.jpeg "Menú Actualizaciones")
</center>

