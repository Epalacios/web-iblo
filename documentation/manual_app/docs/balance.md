# Módulo de Balance
<center>
![Menú Actualizaciones](/img/balance.jpeg "Menú Actualizaciones")
</center>

El módulo de balance permite visualizar el nombre de la Wallet así como también los últimos 10 pagos recibidos y solicitados.

<center>
![Menú Actualizaciones](/img/balance1.jpeg "Menú Actualizaciones")
</center>

Este botón nos permite ver detalles de la operación.

<center>
![Menú Actualizaciones](/img/balance_boton.jpeg "Menú Actualizaciones")
</center>

Al dar clic al botón se mostrará una pantalla emergente que se verá las siguientes:

1. Pagos recibidos (QR)

<center>
![Menú Actualizaciones](/img/balance3.jpeg "Menú Actualizaciones")
</center>

2. Pagos enviados (Correo electrónico, SMS, WhatsApp)

<center>
![Menú Actualizaciones](/img/balance2.jpeg "Menú Actualizaciones")
</center>