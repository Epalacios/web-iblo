# Módulo de Ingreso al Sistema

Para acceder a la aplicación móvil de Iblo, ingrese su Usuario o Correo Electrónico y su Contraseña.

<center>
![Menú Actualizaciones](/img/login.jpeg "Menú Actualizaciones")
</center>

Una vez ingresado los datos solicitados deberá dar clic a inicio de sesión.

Si sus datos son correctos, la plataforma le da el acceso al sistema.

<center>
![Menú Actualizaciones](/img/inicio_sesion.jpeg "Menú Actualizaciones")
</center>

Si el usuario es nuevo en la plataforma de Iblo App, se visualizará la siguiente pantalla, especificando que el usuario no posee wallet asociada ni importada.

Para crear una Wallet desde la aplicación deberá dar clic en el botón Crear

<center>
![Menú Actualizaciones](/img/importar_wallet.jpeg "Menú Actualizaciones")
</center>

La aplicación muestra una pantalla confirmando si desea crear una nueva Wallet, deberá dar clic en el botón Continuar.

<center>
![Menú Actualizaciones](/img/importar_wallet2.jpeg "Menú Actualizaciones")
</center>

**El paso 1**: Se encarga de generar las palabras semillas, deberá guardarlas en el orden especificado. Para seguir con el proceso deberá dar clic en el botón Continuar,

<center>
![Menú Actualizaciones](/img/importar_wallet3.jpeg "Menú Actualizaciones")
</center>

La aplicación envía una pantalla de confirmación, deberá confirmar que guardó las palabras semillas y presionar el botón Continuar

<center>
![Menú Actualizaciones](/img/importar_wallet4.jpeg "Menú Actualizaciones")
</center>

**El paso 2**: Se encarga de confirmar las frases semillas, deberá incluir dichas palabras semillas en el mismo orden especificado en el paso anterior.

<center>
![Menú Actualizaciones](/img/importar_wallet5.jpeg "Menú Actualizaciones")
</center>

**El paso 3**: Se encarga de solicitar el nombre de etiqueta de la Wallet y la contraseña.

**Etiqueta de la Wallet**: Nombre común de reconocimiento de la Wallet en la App.

<center>
![Menú Actualizaciones](/img/importar_wallet6.jpeg "Menú Actualizaciones")
</center>

Al dar clic en botón Crear Wallet deberá esperar unos segundos para la creación de la Wallet

<center>
![Menú Actualizaciones](/img/importar_wallet7.jpeg "Menú Actualizaciones")
</center>

Una vez creada la Wallet la aplicación ingresará al Home para empezar a utilizar las funciones activas en la App. Cada vez que ingrese a Iblo App desde un terminal se conectará a la Wallet que tiene asociada.

<center>
![Menú Actualizaciones](/img/home.jpeg "Menú Actualizaciones")
</center>

**Nota**: Si desea restaurar la Wallet en cualquier otro aplicación deberá tener a la mano las palabras semillas antes indicadas, de perderse dichas palabras semillas no se podrá restaurar la Wallet, por ende, la responsabilidad de la custodia de la semilla es del usuario que se está registrando en el App, Iblo no guarda ninguna clave privada de ninguna Wallet.