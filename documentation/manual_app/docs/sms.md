# Módulo de SMS

<center>
![Menú Actualizaciones](/img/SMS.jpeg "Menú Actualizaciones")
</center>

# Módulo de Pago por mensaje de texto (sms).

Este módulo se encarga de generar un pago de BTC por mensaje de texto (SMS)

Para realizar un pago por mensaje de texto (SMS) deberá ingresar el número de teléfono (solo se aceptan números de teléfono de U.S.A.) y el monto en dólares o en BTC.

<center>
![Menú Actualizaciones](/img/pago_sms.jpeg "Menú Actualizaciones")
</center>

Una vez ingresado el número de teléfono y el monto deberá dar clic en enviar, la aplicación móvil de Iblo mostrará la siguiente pantalla solicitando una confirmación.

Si el usuario está de acuerdo con la solicitud de pago deberá dar clic en enviar

<center>
![Menú Actualizaciones](/img/pago_sms1.jpeg "Menú Actualizaciones")
</center>

Seguidamente, la aplicación enviará un mensaje al número indicado previamente.

<center>
![Menú Actualizaciones](/img/pago_sms22.jpeg "Menú Actualizaciones")
</center>

Si el mensaje fue enviado satisfactoriamente, se mostrará este mensaje.

<center>
![Menú Actualizaciones](/img/correo_electronico2.jpeg "Menú Actualizaciones")
</center>

El usuario deberá ingresar a la bandeja de mensajes y encontrará un mensaje de texto de Iblo con la solicitud de pago.

<center>
![Menú Actualizaciones](/img/pago_sms2.jpeg "Menú Actualizaciones")
</center>


Deber dar clic en el botón Pagar, una vez presione el botón se enviará a la pasarela de pago de Iblo.

<center>
![Menú Actualizaciones](/img/correo_electronico6.png "Menú Actualizaciones")
</center>

Se tendrá un plazo de 30:00 minutos para realizar el pago.

Una vez verificado el pago, se mostrará este mensaje.

<center>
![Menú Actualizaciones](/img/correo_electronico7.png "Menú Actualizaciones")
</center>