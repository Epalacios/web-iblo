# Módulo de pago por QR.
<center>
![Menú Actualizaciones](/img/pagoQR.jpeg "Menú Actualizaciones")
</center>

Este módulo se encarga de generar un pago de BTC mediante un QR.

Para realizar un pago debemos ingresar el monto en dólares o en BTC.

<center>
![Menú Actualizaciones](/img/pago_QR1.jpeg "Menú Actualizaciones")
</center>

Una vez ingresado el monto a pagar se deberá dar clic al botón Verificar Pago.

<center>
![Menú Actualizaciones](/img/verificar.jpeg "Menú Actualizaciones")
</center>

Si es pago fue realizado exitosamente se mostrará la siguiente pantalla.

<center>
![Menú Actualizaciones](/img/pago.png "Menú Actualizaciones")
</center>

Al verificar el pago presioné el botón Ok, la aplicación retornará al módulo de recibir pago por QR