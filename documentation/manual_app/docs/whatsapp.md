# Módulo de WhatsApp

<center>
![Menú Actualizaciones](/img/whatsapp.jpeg "Menú Actualizaciones")
</center>

## Módulo de pago por WhatsApp.

Este módulo se encarga de generar un pago de BTC por WhatsApp.

Para realizar un pago por WhatsApp deberá ingresar el número de teléfono y el monto en dólares o en BTC.

<center>
![Menú Actualizaciones](/img/whatsapp2.jpeg "Menú Actualizaciones")
</center>

Una vez ingresado el número de teléfono y el monto deberá dar clic en enviar, la aplicación móvil de Iblo mostrará la siguiente pantalla solicitando una confirmación.

Si el usuario está de acuerdo con la solicitud de pago, deberá dar clic en enviar

<center>
![Menú Actualizaciones](/img/whatsapp3.jpeg "Menú Actualizaciones")
</center>

Seguidamente, la aplicación enviará un mensaje al número de WhatsApp indicado previamente.

<center>
![Menú Actualizaciones](/img/whatsapp4.jpeg "Menú Actualizaciones")
</center>

Si el mensaje fue enviado satisfactoriamente, se mostrará este mensaje.

<center>
![Menú Actualizaciones](/img/correo_electronico2.jpeg "Menú Actualizaciones")
</center>

El usuario deberá ingresar al WhatsApp y encontrará un WhatsApp de Iblo con la solicitud de pago.

<center>
![Menú Actualizaciones](/img/whatsapp5.jpeg "Menú Actualizaciones")
</center>

Deber dar clic en el botón Pagar, una vez presione el botón se enviará a la pasarela de pago de Iblo.

<center>
![Menú Actualizaciones](/img/correo_electronico6.png "Menú Actualizaciones")
</center>

Se tendrá un plazo de 30:00 minutos para realizar el pago.

Una vez verificado el pago, se mostrará este mensaje.

<center>
![Menú Actualizaciones](/img/correo_electronico7.png "Menú Actualizaciones")
</center>